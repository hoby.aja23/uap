package Contest;
//Interface HargaTiket merupakan sebuah kontrak atau 
//blueprint yang menyatakan bahwa kelas-kelas yang mengimplementasikan 
//interface ini harus menyediakan implementasi untuk metode hitungHarga().

interface HargaTiket {
    
    double hitungHarga();

}