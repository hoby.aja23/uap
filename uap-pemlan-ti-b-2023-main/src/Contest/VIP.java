package Contest;

class VIP extends TiketKonser {
   // Konstraktor VIP memiliki uda parameter yaitu String nama dan double harga
    // pernyataan super digunakan untuk memanggil konstruktor dari superclass (kelas induk) VIP dengan 
    // meneruskan nilai nama dan harga ke konstruktor superclass tersebut.
    public VIP(String nama, double harga) {
        super(nama, harga);
    }
    //Override digunakan untuk mewarisi method  
    //dari class induk yang digunakan VIP adalah Tiket konser.
    //Ketika sebuah subclass meng-override sebuah method dari superclass, 
    //artinya subclass tersebut menyediakan implementasi yang berbeda untuk method tersebut. 
    @Override
    public double hitungHarga() {
        return harga;
    }
}