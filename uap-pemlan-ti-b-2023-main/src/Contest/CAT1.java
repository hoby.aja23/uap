package Contest;

class CAT1 extends TiketKonser {
    // Konstraktor CAT1 memiliki uda parameter yaitu String nama dan double harga
    // pernyataan super digunakan untuk memanggil konstruktor dari superclass (kelas induk) CAT1 dengan 
    // meneruskan nilai nama dan harga ke konstruktor superclass tersebut.
    public CAT1(String nama, double harga) {
        super(nama, harga);
    }
    //Override digunakan untuk mewarisi method  
    //dari class induk yang digunakan CAT1 adalah Tiket konser.
    //Ketika sebuah subclass meng-override sebuah method dari superclass, 
    //artinya subclass tersebut menyediakan implementasi yang berbeda untuk method tersebut. 
   
    @Override
    public double hitungHarga() {
        return harga;
    }




}