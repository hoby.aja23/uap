package Contest;

abstract class TiketKonser implements HargaTiket {
    //Menggunakan abstract kelas yg di implementasikan dari hargatiket
    // protected digunakan sebagai modifier akses yang dapat diterapkan pada 
    //variabel, method, dan konstruktor dalam sebuah kelas. 

    //Kelas TiketKonser memiliki properti nama dan harga yang diinisialisasi melalui konstruktor. 
    //Terdapat juga metode getNama() untuk mengembalikan nilai properti nama. 
    //Selain itu, terdapat metode abstrak hitungHarga() yang harus diimplementasikan oleh kelas turunannya.
        protected String nama;
        protected double harga;
    
        public TiketKonser(String nama, double harga) {
            this.nama = nama;
            this.harga = harga;
        }
    
        public String getNama() {
            return nama;
        }
    
        public abstract double hitungHarga();
}