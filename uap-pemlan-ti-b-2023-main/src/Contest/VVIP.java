package Contest;

class VVIP extends TiketKonser {
    // Konstraktor VVIP memiliki uda parameter yaitu String nama dan double harga
    // pernyataan super digunakan untuk memanggil konstruktor dari superclass (kelas induk) VVIP dengan 
    // meneruskan nilai nama dan harga ke konstruktor superclass tersebut.
    public VVIP(String nama, double harga) {
        super(nama, harga);
    }
    //Override digunakan untuk mewarisi method dari class induk yang digunakan VVIP 
    //yaitu adalah Tiket konser.
    //Ketika sebuah subclass meng-override sebuah method dari superclass, 
    //artinya subclass tersebut menyediakan implementasi yang berbeda untuk method tersebut. 
    @Override
    public double hitungHarga() {
        return harga;
    }
}