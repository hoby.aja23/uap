package Contest;

class PemesananTiket {
    // Kode tersebut adalah bagian dari sebuah kelas yang memiliki array tiketKonser 
    //yang berisi beberapa objek tiket konser. Setiap objek tiket konser diinisialisasi 
    //dengan nama dan harga yang sesuai menggunakan konstruktor kelas yang sesuai.

    //Metode pilihTiket(int index) digunakan untuk memilih tiket konser berdasarkan indeks yang diberikan. 
    //Metode ini mengembalikan objek tiket konser yang berada pada indeks yang diinputkan dari array tiketKonser.

    //Dengan menggunakan array tiketKonser dan metode pilihTiket(int index), 
    //Anda dapat memilih tiket konser yang diinginkan berdasarkan indeksnya.





    private static TiketKonser[] tiketKonser;

    static {
        tiketKonser = new TiketKonser[]{
            new CAT8("CAT 8", 900000),
            new CAT1("CAT 1", 4500000),
            new FESTIVAL("FESTIVAL", 6000000),
            new VIP("VIP", 8000000),
            new VVIP("UNLIMITED EXPERIENCE", 13000000)
        };
    }

    public static TiketKonser pilihTiket(int index) {
        return tiketKonser[index];
    }
}