/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */

package Contest;

import java.util.*;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) {
        //Di dalam blok try, terdapat kondisi yang memeriksa panjang namaPemesan. 
        //Jika panjang nama pemesan melebihi 10 karakter, maka program akan melempar 
        //pengecualian InvalidInputException dengan pesan "Panjang nama pemesan maksimal 10 karakter!".
        
        Scanner scanner = new Scanner(System.in);

        System.out.println("Selamat datang di Pemesanan Tiket Konser Coldplay!");

        try {
            System.out.print("Masukkan nama pemesan: ");
            String namaPemesan = scanner.nextLine();

            if (namaPemesan.length() > 10) {
                throw new InvalidInputException("Panjang nama pemesan maksimal 10 karakter!");
            }
            // Untuk memeberikan perulangan jika user salah memasukan input.
            // Dengan menggunakan while, dan tidak lupa menggunakan exeption
            //Jika panjang nama pemesan melebihi 5 karakter, maka InvalidInputException akan dilempar.
            System.out.println("Pilih jenis tiket:");
            System.out.println("1. CAT 8");
            System.out.println("2. CAT 1");
            System.out.println("3. FESTIVAL");
            System.out.println("4. VIP");
            System.out.println("5. UNLIMITED EXPERIENCE");
            System.out.print("Masukkan pilihan: ");

            int choice=0;
            boolean validChoice = false;

            while (!validChoice) {
                System.out.print("Masukkan pilihan: ");
                try {
                    choice = Integer.parseInt(scanner.nextLine());
                    if (choice >= 1 && choice <= 5) {
                        validChoice = true;
                    } else {
                        System.out.println("Input tidak valid! Pilihan hanya 1-5.");
                    }
                } catch (NumberFormatException e) {
                    System.out.println("Input tidak valid! Masukkan pilihan tiket dengan angka.");
                }
            }
            // Untuk mencetak receipt yang telah disediakan
            TiketKonser tiket = PemesananTiket.pilihTiket(choice - 1);

            String kodeBooking = generateKodeBooking();

            String tanggalPesanan = getCurrentDate();

            System.out.println("\n----- Detail Pemesanan -----");
            System.out.println("Nama Pemesan: " + namaPemesan);
            System.out.println("Kode Booking: " + kodeBooking);
            System.out.println("Tanggal Pesanan: " + tanggalPesanan);
            System.out.println("Tiket yang dipesan: " + tiket.getNama());
            System.out.println("Total harga: " + tiket.hitungHarga() + " USD");
        } catch (InvalidInputException e) {
            System.out.println("Terjadi kesalahan: " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Terjadi kesalahan: " + e.getMessage());
        } finally {
            scanner.close();
        }
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}